The [Data Logging Service (DLS)](https://etherlab.org/en/dls) is a data logging
system for [EtherLab](https://etherlab.org/en), that is capable of collecting,
compressing and storing high-frequency realtime data.  The goal is, to allow
the user unlimited and performant access to the stored data. A signal overview
over a year is retrieved as fast as a tiny signal fluctuation, that happened in
a fraction of a second.

Documentation is available in German and French language. See the
[documentation directory](doku).
